# 360 Panorama Downloader
![Private](https://img.shields.io/badge/-private-important)
![Version](https://img.shields.io/badge/version-v1.0.1-blue)
![Not maintained](https://img.shields.io/maintenance/no/2021)

Downloads panoramas from 360.io

**NOT MAINTAINED SINCE THE END OF SUPPORT OF 360 PANORAMA APP**

## How to use

* Clone the repository and install dependencies
* Create file `panoramas.json` with following content: 

```
[
  {
    "id": "<ID of panorama (get from URL - https://360.io/{ID})>"
    "name": "<folder name where panorama will be stored, e.g. '2020-01-01_1'>"
  },
  ...
]
```

* Start the application via `npm start`.

All panoramas will be downloaded to the folder `storage` in root of the application.

## Folder structure

* `<name>/flat.jpg` - flattened panorama
* `<name>/stereographic.jpg` - spherical flattened panorama
* `<name>/sphere/N.jpg` - parts of 360º photosphere (may be used later when someone creates a custom viewer for it)