import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';
import pq from 'p-queue';

const PQueue = pq.default;

const ROOT = './storage';
const P360_ROOT = 'https://360.io/images/viewer/';
const P360_FLAT = '_flat.jpg';
const P360_THUMBNAIL = '_flat_small.jpg';
const P360_STEREO = '_stereographic.jpg';

const queue = new PQueue({concurrency: 10});
queue.on('add', () => {
  console.log(`Task is added.  Size: ${queue.size}  Pending: ${queue.pending}`);
});
queue.on('next', () => {
  console.log(`Task is completed.  Size: ${queue.size}  Pending: ${queue.pending}`);
});

const _download = async (url, target, filename) => {
  if (!fs.existsSync(target)) {
    fs.mkdirSync(target, { recursive: true });
  }
  const fullPath = path.resolve(target, filename);

  const writer = fs.createWriteStream(fullPath);
  const response = await fetch(url, { method: 'GET' });
  response.body.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', async () => {
      await writer.close();
      resolve(fullPath);
    });
    writer.on('error', err => {
      fs.unlinkSync(fullPath);
      reject(err);
    });
  });
}

const downloadPanorama = panorama => {
  const mapping = [];
  mapping.push({
    url: P360_ROOT + panorama.id + P360_FLAT,
    target: path.resolve(ROOT, panorama.name),
    filename: 'flat.jpg'
  }, {
    url: P360_ROOT + panorama.id + P360_STEREO,
    target: path.resolve(ROOT, panorama.name),
    filename: 'stereographic.jpg'
  }, {
    url: P360_ROOT + panorama.id + P360_THUMBNAIL,
    target: path.resolve(ROOT, panorama.name),
    filename: 'thumbnail.jpg'
  });

  for (let i = 0; i <= 5; i++) {
    mapping.push({
      url: P360_ROOT + panorama.id + i + '.jpg',
      target: path.resolve(ROOT, panorama.name, 'sphere'),
      filename: i + '.jpg'
    });
  }

  mapping.forEach(async pano => {
    await queue.add(async () => await _download(pano.url, pano.target, pano.filename));
  });
};

const main = () => {
  const panoramas = JSON.parse(fs.readFileSync('./panoramas.json').toString());
  panoramas.forEach(panorama => downloadPanorama(panorama));
}

main();